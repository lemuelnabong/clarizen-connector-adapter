GETTING STARTED
Clarizen provides programmatic access to your organization information using a simple and powerful REST API.

Using the Clarizen REST API you are able to develop various types of applications, such as:

Custom applications: create custom applications to gain additional benefits for your organization using business data located in the Clarizen repository. Provide your employees with specific and familiar User Interface for specific organizational processes
Integration with Desktop tools: create integration with desktop authoring tools to bring task management closer to the employee's desktop
Integration with legacy systems:  create integration with your internal data management systems to exchange relevant data and perform on the fly integration

INTRODUCING THE CLARIZEN REST API
REST API access is enabled for all Enterprise organizations and does not require a special key to access the API.

Certified partners who develop applications that need to work for Professional edition organizations as well can request a partner ID to access the API. Please contact us for more information.


Supported Editions
REST API is provided for Clarizen V6 and on. To use the REST API your organization must use Enterprise Edition. If you are already a customer of Clarizen and do not have the enterprise edition, please, contact your sales representative.

Business and development partners of Clarizen can use the Developer edition. Clarizen's developer edition provides access to all functions available in the Enterprise edition.


REST API BASICS
Clarizen REST resources are divided into several core services: Authentication, Metadata, Data and Bulk. The base URL for all REST API calls is https://api.clarizen.com/v2.0/services/.
Each core service resides under the base URL so, for example, calls that are part of the Authentication service will reside under https://api.clarizen.com/v2.0/services/authentication.
Some resources support multiple HTTP methods (i.e. GET, POST, PUT and DELETE) to perform different operations on the same resource (such as retrieving an entity vs updating an entity) and some resource support only a single method (i.e. GET only). The supported format for both requests and responses is JSON.

from : http://usermanual.clarizen.com/rest-api-guide