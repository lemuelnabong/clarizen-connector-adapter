package io.toro.integrate.clarizen

import com.toro.esb.core.api.APIResponse
import com.toro.esb.core.service.annotation.ValueOptions
import org.hibernate.validator.constraints.NotBlank
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RequestBody

import javax.validation.constraints.Size
import javax.ws.rs.DefaultValue

import io.toro.clarizen.model.common.*
import io.toro.clarizen.model.request.*
import io.toro.clarizen.model.response.*

@RestController
@RequestMapping('clarizen')
public class ClarizenAdapter implements Serializable {

    private static String getMultipleParam ( String field ) {
		String str
		str.StringUtils.join( field.replaceAll("\\s",""), ',' )
    }
   
    /*
     * Authentication - login 
     */
    @RequestMapping( value = "authentication/login", method = [RequestMethod.POST], produces = ['application/json', 'application/xml'])
    APIResponse login (
        @RequestParam String alias,
        @RequestBody AccessCredentialRequest requestBody ){
        return new APIResponse( 'OK', alias.clarizenLogin( requestBody ))
    }

	/*
	 * Data - objectsDataPut
	 */
	@RequestMapping( value="data/objects/{typeName}", method=[RequestMethod.PUT], produces=['application/json'] )
	APIResponse objectsDataPut (
		@RequestParam( required=false ) String alias,
		@PathVariable( 'typeName' ) String typeName,
		@RequestParam String objectFieldKey,
		@RequestParam String objectFieldValue ){
			return new APIResponse( 'OK', alias.clarizenObjectsDataPut( typeName, objectFieldKey, objectFieldValue ))
		}

    /*
     * Data - objectsDataPost
     */
    @RequestMapping( value="data/objects/{typeName}/{entityId}", method=[RequestMethod.POST], produces=['application/json'] )
    APIResponse objectsDataPost (
        @RequestParam( required=false ) String alias,
        @PathVariable( 'typeName' ) String typeName,
        @PathVariable( 'entityId' ) String entityId,
        @RequestParam String objectFieldKey,
        @RequestParam String objectFieldValue ){
            return new APIResponse( 'OK',
                alias.clarizenObjectsDataPost( typeName, entityId, objectFieldKey, objectFieldValue ))
        }

	/*
	 * Data - objectsDataGet
	 */
	@RequestMapping( value="data/objects/{typeName}/{entityId}", method=[RequestMethod.GET], produces=['application/json'] )
	APIResponse objectsDataGet (
		@RequestParam( required=false ) String alias,
		@PathVariable( 'typeName' ) String typeName,
		@PathVariable( 'entityId' ) String entityId,
		@RequestParam String fields ){
			return new APIResponse( 'OK',
				alias.clarizenObjectsDataGet( typeName, entityId, getMultipleParam( fields )))
		}

}