<%@ page import="com.toro.esb.core.esbpackage.model.ESBPackage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%
    ESBPackage esbPackage = (ESBPackage)getServletConfig().getServletContext().getAttribute( "ESB_PACKAGE" );
    String packageName = esbPackage.getName();
%>

<head>
    <meta charset="utf-8">
    <title>TORO INTEGRATE Clarizen Adapter</title>
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/assets/css/uikit/uikit.css">
    <link rel="stylesheet" href="/assets/css/uikit/uikit-components.css">
    <link rel="stylesheet" href="/assets/css/uikit/uikit-docs.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700">
    <link rel="stylesheet" href="/assets/css/bootstrap/datetimepicker.css">
    <link rel="stylesheet" href="/assets/css/layout-esb.css">
    <link rel="stylesheet" href="/assets/css/layout-error_404.css">
    <!-- jQuery and jQueryUI -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- globalize for graphs -->
    <script src="//ajax.aspnetcdn.com/ajax/globalize/0.1.1/globalize.min.js"></script>
    <script src="/assets/js/vendor/jquery.flot.js"></script>
    <script src="/assets/js/vendor/uikit-flot.js"></script>
    <script src="/assets/js/plugins/moment.js"></script>
    <!-- Bootstrap Plugins -->
    <script src="/assets/js/bootstrap/bootstrap.js"></script>
    <script src="/assets/js/bootstrap/datetimepicker.min.js"></script>
    <!-- UIKit Plugins -->
    <script src="/assets/js/plugins/prettify.js"></script>
    <script src="/assets/js/plugins/plugin.js"></script>
    <script src="/assets/js/plugins/tableSorter.js"></script>
    <script src="/assets/js/plugins/pagination.js"></script>
    <script src="/assets/js/plugins/sticky-navbar.js"></script>
    <script src="/assets/js/plugins/jquery.tooltipster.min.js"></script>
    <script src="/assets/js/plugins/hoverChart.js"></script>
    <script src="/assets/js/docs.js"></script>
    <script src="/assets/esb/js/esb.js"></script>
</head>
<body style="position: relative;">
<div class="bs-docs-header" id="content" <%--style="background-image: linear-gradient(to bottom, #036b92 0%, #0080b0 100%);"--%>>
    <div class="container" id="top">
        <h1 style="width: 100%">TORO INTEGRATE - Clarizen Adapter</h1>

        <p>Read below on how to write beautiful code that seamlessly integrates the TORO INTEGRATE with Clarizen.</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-9" role="main">
            <div class="bs-docs-section">
                <h1 id="requirements" class="page-header">Requirements</h1>
                <p>
                    Active Clarizen Account. <a href="https://app2.clarizen.com/Clarizen/Pages/Service/Login.aspx">Register here</a>
                </p>
            </div>
            <div class="bs-docs-section">
                <h1 id="authentication" class="page-header">Authentication</h1>
                <p>
                    The Authentication service is the starting point for all applications working with the Clarizen REST API.
                    The basic sequence to start working with the API is to first determine in which Clarizen data center your organization data is located, then perform a login operation to that data center to receive a Session ID.
                    The Session ID is returned in the response body, each additional request should send this session ID back to the server in an "Authorization" header with the “Session” prefix in order to reuse the same Session.
                </p>
                <h4>Login</h4>
                    <p>Login to the API</p>
                        <pre class="prettyprint lang-groovy linenums">
                            'toroPropertyAlias'.clarizenLogin( 'clarizenUserName@email.com', 'mYpAssWord' )
                        </pre>
                <h4>Get Session Information</h4>
                    <p>Returns information about the current session</p>
                        <pre class="prettyprint lang-groovy linenums">
                            'toroPropertyAlias'.clarizenGetSessionInfo()
                        </pre>
                <h4>Logout</h4>
                    <p>Terminate the current API session</p>
                        <pre class="prettyprint lang-groovy linenums">
                            'toroPropertyAlias'.clarizenLogout()
                        </pre>
            </div>
            <div class="bs-docs-section">
                <h1 id="metadata" class="page-header">Metadata</h1>
                <p>
                    The Metadata service provides methods to interrogate the Clarizen data model.
                    You can use this service to get a list of entity types available to your organization and to get a detailed description of the fields available on each entity type.
                    The following example retrieves the description of all entities in your organization
                </p>
                <h4>Describe Metadata</h4>
                    <p>Returns information about the entity types in your organization </p>
                    <pre class="prettyprint lang-groovy linenums">
                        'toroPropertyAlias'.clarizenDescribeMetadata()
                    </pre>
            </div>
            <div class="bs-docs-section">
                <h1 id="data" class="page-header">Data</h1>
                <p>
                    The Data service contains most of the functionality available via the REST API which is to Create, Retrieve, Update and Delete data in Clarizen. The basic data element in Clarizen is an Entity which is accessible via the /services/data/objects/{typeName}/{id} URI where typeName is the Entity Type (e.g. Project or Issue} and id is the id of that entity. This URI can be used to perform the basic 4 CRUD operations via the following respective HTTP methods: PUT, GET, POST and DELETE. The following examples show how these methods can used on an entity of type “Issue”
                </p>
                <h4>Objects -PUT</h4>
                    <p>Create an entity</p>
                    <pre class="prettyprint lang-groovy linenums">
                        'toroPropertyAlias'.clarizenObjectsDataPut( 'issue', 'title', 'IssueTitle' )
                        </pre>
                <h4>Objects -GET</h4>
                    <p>Get the entity</p>
                    <pre class="prettyprint lang-groovy linenums">
                        'toroPropertyAlias'.clarizenObjectsDataGet( 'issue', '2945bqapb0y6tbl4ys1ystk5j3', 'CreatedOn,CreatedBy,CreatedBy.Name' )
                    </pre>
                <h4>Objects -POST</h4>
                    <p>Update the entity</p>
                    <pre class="prettyprint lang-groovy linenums">
                        'toroPropertyAlias'.clarizenObjectsDataPost( 'issue', '2945bqapb0y6tbl4ys1ystk5j3', 'Priority', '5' )
                    </pre>
                <h4>Objects -DELETE</h4>
                    <p>Delete the entity</p>
                    <pre class="prettyprint lang-groovy linenums">
                        'toroPropertyAlias'.clarizenObjectsDataDelete( 'issue', entityId )
                    </pre>
                <h4>Query</h4>
                    <p>Get the entity</p>
                    <pre class="prettyprint lang-groovy linenums">
                        'toroPropertyAlias'.clarizenQuery ( new QueryRequest(
                            'q': 'SELECT createdOn,title FROM Issue WHERE createdOn>2015-07-21' ))
                    </pre>
            </div>
            <div class="bs-docs-section">
                <h1 id="bulk" class="page-header">Bulk</h1>
                <p>
                    The bulk service allows clients to perform several API calls in a single HTTP round trip. The bulk service can be used in two scenarios:

                    1. Performance optimization
                    2. Overcoming HTTP limitations
                    
                    The bulk service exposes a single Execute operation which accepts an array of “Request” objects and returns an array of “Response” objects. Each request represents a single REST API call and has the following properties: Url, Method and Body. Each response has a StatusCode and Body. For example, to update the ‘StartDate’ and ‘DueDate’ of a task and immediately retrieve the updated ‘Duration’ of that task, the following request can be performed:
                </p>
                <h4>Execute</h4>
                    <p>Allows executing several API calls in a single round trip to the server</p>
                        <pre class="prettyprint lang-groovy linenums">
                            'toroPropertyAlias'.clarizenExecute( alias, new ExecuteRequest( 
                                requests:[
                                        new Request([
                                            url     :'/data/objects/Issue/6rogf9gdi2imf35es99lznp2s0',
                                            method  : 'POST',
                                            body    : '{title: "5"}' ]),
                                        new Request([
                                            url     : '/data/objects/Issue/6rogf9gdi2imf35es99lznp2s0?fields=duration',
                                            method  : 'GET']),
                                    ]))
                        </pre>
            </div>
        </div>
        <div class="col-md-3">
            <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix-top" role="complementary">
                <ul class="nav bs-docs-sidenav">
                    <li class="active"><a href="#requirements">Overview</a></li>
                    <li>
                        <a href="#authentication">Authentication</a>
                    </li>
                    <li>
                        <a href="#metadata">Metadata</a>
                    </li>
                    <li>
                        <a href="#data">Data</a>
                    </li>
                    <li>
                        <a href="#bulk">Bulk</a>
                    </li>
                </ul>
                <a class="back-to-top" href="#top">
                    Back to top
                </a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.prettyPrint && prettyPrint();
</script>

</body>
</html>