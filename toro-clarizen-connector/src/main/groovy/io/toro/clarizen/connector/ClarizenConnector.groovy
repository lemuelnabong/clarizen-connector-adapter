package io.toro.clarizen.connector

import groovyx.net.http.ContentType
import groovy.transform.CompileStatic
import groovyx.net.http.HttpResponseException
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import groovy.json.JsonSlurper
import com.toro.licensing.LicenseManager
import io.toro.clarizen.model.request.*
import io.toro.clarizen.model.response.*
import io.toro.clarizen.model.common.*
import io.toro.clarizen.exception.ClarizenException
import net.sf.json.util.PropertyFilter
import java.nio.charset.Charset
import net.sf.json.JsonConfig
import net.sf.json.JSONObject
import org.apache.http.entity.StringEntity
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * https://api.clarizen.com/V2.0/services/
 * Added @CompileStatic annotation, means the code needs to be more verbose
 */

@SuppressWarnings([ 'FieldName', 'PropertyName' ])
class ClarizenConnector {
	private static final String connectorName = 'clarizen'
	private static final String baseUrl = 'https://api2.clarizen.com/'
	private static String sessionId
	private static final Map sessions = [:]
	private static String alias
	private static Boolean loginSequenceOn

	@SuppressWarnings(['UnnecessaryGetter'])
	private static String getProperties( String name ) {
		name.getTOROProperty()
	}

	private static Map modelToMap( Object bean ) {
		new ObjectMapper().convertValue( bean, Map )
	}

	@SuppressWarnings([
		'UnnecessaryGetter',
		'DuplicateStringLiteral',
		'NoDef'
	])
	private static Map jsonToroProperties () {
		String toroProperty = getProperties("$connectorName.${alias}")
		if ( toroProperty ) {
			return new JsonSlurper().parseText( toroProperty ) as Map
		}
		else if ( this.loginSequenceOn )
			return [:]
	}

	@SuppressWarnings([
		'DuplicateStringLiteral',
		'NoDef'
	])
	private static Map saveEntryToToroProperty ( String key, Object val ) {
		def toroPropertiesJson = new JSONObject( jsonToroProperties() )
		toroPropertiesJson.put( key, val )
		"$connectorName.${alias}".saveTOROProperty( toroPropertiesJson.toString() )
		toroPropertiesJson
	}

	private static Boolean sessionCurrentMatchesSessionAlias() {
		if ( !loginSequenceOn )
			sessions."$alias" == ClarizenConnector.sessionId
	}

	@SuppressWarnings([
		'UnnecessaryGetter',
		'DuplicateStringLiteral'
	])
	private static RESTClient trimEmptyRequestField() {
		RESTClient restClient = new RESTClient( baseUrl )
		restClient.encoder.'application/json' = { body, contentType ->
			JsonConfig config = new JsonConfig()
			config.setJsonPropertyFilter( new PropertyFilter() {
						@SuppressWarnings( 'UnnecessaryIfStatement' )
						@SuppressWarnings( 'UnusedMethodParameter')
						boolean apply( Object source, String name, Object value ) {
							if ( value == null || ( value instanceof String
							&& value.toString().trim().length() == 0 ) )
								return true
							false
						}
					})
			JSONObject json = JSONObject.fromObject( body, config )
			StringEntity entity = new StringEntity( json.toString(), Charset.defaultCharset() )
			entity.setContentType( contentType.toString() )
			entity
		}
		restClient
	}

	@SuppressWarnings([ 'UnnecessaryGetter', 'DuplicateStringLiteral' ])
	private static Map sendRequest( String path, HttpMethod httpMethod, Object request = [:] ) {
		LicenseManager.getInstance().getLicense()
		RESTClient restClient = trimEmptyRequestField()

		Map params = [ path: path,
			requestContentType: ContentType.JSON,
			headers: [ 'Authorization': 'Session ' + ClarizenConnector.sessionId,
				'Accept': ['text/json' ]]]

		HttpResponseDecorator response

		if ( httpMethod == HttpMethod.delete ) {
			params.query = request as Serializable
			response = restClient.delete(params) as HttpResponseDecorator
		}
		else if ( httpMethod == HttpMethod.get ) {
			params.query = request as Serializable
			response =  restClient.get( params ) as HttpResponseDecorator
		}
		else if ( httpMethod == HttpMethod.post ) {
			params.body = request as Serializable
			response =  restClient.post( params ) as HttpResponseDecorator
		}
		else if ( httpMethod == HttpMethod.put ) {
			params.body = request as Serializable
			response =  restClient.put( params ) as HttpResponseDecorator
		}

		Map json = [ : ]
		json = response.data as Map

		if ( json?.message != null )
			throw new ClarizenException( json.message as String )

		json
	}

	@SuppressWarnings('DuplicateStringLiteral')
	private static Map handleInvalidSession ( path, httpMethod, request ) {
		//user must relogin and redirect to method destination
		if ( this.sessionId && getProperties( "$connectorName.${alias}" )) {
			AccessCredentialRequest model = new AccessCredentialRequest()
			model.userName = jsonToroProperties().get( 'userName' )
			model.password = jsonToroProperties().get( 'password' )
			clarizenLogin( alias, model )
			sendRequest( path, httpMethod, request )
		}
		else {
			if ( !getProperties( "$connectorName.${alias}" ) )
				throw new ClarizenException( 'InvalidTOROAlias' )

			else if ( !this.sessionId )
				throw new ClarizenException( 'NoSessionID' )

			else
				throw new ClarizenException( 'Unexpected, found sessionId on unmatching session' )
		}
	}

	private static Map handleRequestSession( String path, HttpMethod httpMethod, Object request = [:] ) {
		//fresh login, non-login but valid session
		if ( loginSequenceOn || sessionCurrentMatchesSessionAlias() )
			sendRequest( path, httpMethod, request )

		else if ( !loginSequenceOn && !sessionCurrentMatchesSessionAlias() )
			handleInvalidSession( path, httpMethod, request )

		else
			throw new ClarizenException( 'Unexpected error on handlingRequest' )
	}

	@SuppressWarnings([
		'ConnectorJavadocRule',
		'DuplicateStringLiteral',
		'UnnecessaryGetter',
		'NoDef'
	])
	private static Map contactClarizenApi ( String alias, HttpMethod httpMethod, String path, Object request = [:]) {
		if ( alias || !alias.isAllWhitespace() )
			ClarizenConnector.alias = alias

		try {
			handleRequestSession( path, httpMethod, request )
		}
		catch ( HttpResponseException ex ) {
			throw new ClarizenException( ex.response.data.errorCode )
		}
	}

	private static String parsePath ( ServiceType serviceType, String methodName, String[] args ) {
		String path = "v2.0/services/${serviceType}/${methodName}"
		for ( String key : args )
			path += "/${key}"
		path
	}

	/**
	 * the Url where your org is located - https://api2.clarizen.com/V2.0/services/authentication/getserverdefinition
	 * @param alias property file where you can save configurations
	 * @param model AccessCredentialRequest
	 * @return GetServerDefinitionResponse
	 */
	@CompileStatic
	static GetServerDefinitionResponse clarizenGetServerDefinition ( String alias, AccessCredentialRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.authentication, 'getServerDefinition' ),
				model ) as GetServerDefinitionResponse
	}

	/**
	 * Returns info about the current session - https://api2.clarizen.com/V2.0/services/authentication/getsessioninfo
	 * @param alias property file where you can save configurations
	 * @param model GetSessionSessionInfoRequest
	 * @return GetSessionInfoResponse
	 */
	@CompileStatic
	@SuppressWarnings([
		'DuplicateStringLiteral',
	])
	static GetSessionInfoResponse clarizenGetSessionInfo ( String alias, GetSessionInfoRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.authentication, 'getSessionInfo' ),
				model ) as GetSessionInfoResponse
	}

	/**
	 * Login to the API - https://api2.clarizen.com/V2.0/services/authentication/login
	 * @param alias property file where you can save configurations
	 * @param request AccessCredentialRequest
	 * @return LoginResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static LoginResponse clarizenLogin( String alias , AccessCredentialRequest request ) {
		loginSequenceOn = true
		Map response = contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.authentication, 'login' ),
				request )
		if ( !response.containsKey( 'errorCode' )) {
			sessions.put( ClarizenConnector.alias, response.sessionId )
			ClarizenConnector.sessionId = response.sessionId
			saveEntryToToroProperty( 'userName', request.userName )
			saveEntryToToroProperty( 'password', request.password )
		}
		loginSequenceOn = false
		response as LoginResponse
	}

	/**
	 * Terminate the current API session - https://api2.clarizen.com/V2.0/services/authentication/logout
	 * @param alias property file where you can save configurations
	 */
	@CompileStatic
	static void clarizenLogout ( String alias ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.authentication, 'logout' ))
	}

	/**
	 * Performs a query and aggreagtes - https://api2.clarizen.com/V2.0/services/data/aggregatequery
	 * @param alias property file where you can save configurations
	 * @param model AggregateQueryRequest
	 * @return AggregateQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static AggregateQueryResponse clarizenAggregateQuery ( String alias, AggregateQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'aggregatequery' ),
				model ) as AggregateQueryResponse
	}
	/**
	 * Changes the state of an object - https://api2.clarizen.com/V2.0/services/data/changeste
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model ChangeStateRequest
	 */
	@CompileStatic
	static void clarizenChangeState ( String alias, ChangeStateRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'changestate' ),
				model )
	}

	/**
	 * Performs a query and returns the result count - https://api2.clarizen.com/V2.0/services/data/countquery
	 * @param alias property file where you can save configurations
	 * @param model CountQueryRequest
	 * @return CountQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static CountQueryResponse clarizenCountQuery ( String alias, CountQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'countquery' ),
				model ) as CountQueryResponse
	}

	/**
	 * Creates and immediatly retrieves an entity - https://api2.clarizen.com/V2.0/services/data/createandretrieve
	 * @param alias property file where you can save configurations
	 * @param model CreateAndRetrieveRequest
	 * @return CreateAndRetrieveResponse
	 */
	@CompileStatic
	static CreateAndRetrieveResponse clarizenCreateAndRetrieve ( String alias, CreateAndRetrieveRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'createandretrieve' ),
				model ) as CreateAndRetrieveResponse
	}

	/**
	 * Creates a discussion message - https://api2.clarizen.com/V2.0/services/data/creatediscussion
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model CreateDiscussionRequest
	 * @return CreateDiscussionResponse
	 */
	@CompileStatic
	static CreateDiscussionResponse clarizenCreateDiscussion (
			String alias,
			CreateDiscussionRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'creatediscussion' ),
				model ) as CreateDiscussionResponse
	}

	/**
	 * Creates an entity from a predefined template - https://api2.clarizen.com/V2.0/services/data/createfromtemplate
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model CreateFromTemplateRequest
	 * @return CreateFromTemplateResponse
	 */
	@CompileStatic
	static CreateFromTemplateResponse clarizenCreateFromTemplate (
			String alias,
			CreateFromTemplateRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'createfromtemplate' ),
				model ) as CreateFromTemplateResponse
	}

	/**
	 * Returns the social feed of an object - https://api2.clarizen.com/V2.0/services/data/entityfeedquery
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model EntityFeedQueryRequest
	 * @return EntityFeedQueryResponse
	 */
	@CompileStatic
	static EntityFeedQueryResponse clarizenEntityFeedQuery (
		String alias,
		EntityFeedQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'entityfeedquery' ),
				model ) as EntityFeedQueryResponse
	}

	/**
	 * Retrieve entities from Clarizen - https://api2.clarizen.com/V2.0/services/data/entityquery
	 * @param alias property file where you can save configurations
	 * @param model EntityQueryRequest
	 * @return EntityQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static EntityQueryResponse clarizenEntityQuery ( String alias, EntityQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'entityquery' ),
				model ) as EntityQueryResponse
	}

	/**
	 * Retrieves expenses for Project or Customer - https://api2.clarizen.com/V2.0/services/data/expensequery
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model ExpenseQueryRequest
	 * @return ExpenseQueryRequest
	 */
	@CompileStatic
	static ExpenseQueryResponse clarizenExpenseQuery (
			String alias,
			String typeName,
			String entityId,
			ExpenseQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'expensequery', typeName, entityId ),
				model ) as ExpenseQueryResponse
	}

	/**
	 * Finds a user based on several criterias - https://api2.clarizen.com/V2.0/services/data/finduserquery
	 * @param alias property file where you can save configurations
	 * @param model FindUserQueryRequest
	 * @return FindUserQueryResponse
	 */
	@CompileStatic
	static FindUserQueryResponse clarizenFindUserQuery ( String alias, FindUserQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'finduserquery' ),
				model ) as FindUserQueryResponse
	}

	/**
	 * Provides information about calendar definitions - https://api2.clarizen.com/V2.0/services/data/getcalendarinfo
	 * @param alias property file where you can save configurations
	 * @param userId clarizen user account
	 * @return GetServerDefinitionRequest
	 */
	@CompileStatic
	static GetCalendarInfoResponse clarizenGetCalendarInfo ( String alias, String userId ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.data, 'getcalendarinfo' ),
				[ userId: userId ]) as GetCalendarInfoResponse
	}

	/**
	 * Returns the list of template - https://api2.clarizen.com/V2.0/services/data/gettemplatedescriptions
	 * @param alias property file where you can save configurations
	 * @param typeName entity typeName
	 * @return GetTemplateDescriptionsResponse
	 */
	@CompileStatic
	static GetTemplateDescriptionsResponse clarizenGetTemplateDescriptions (
			String alias,
			String typeName ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.data, 'gettemplatedescriptions' ),
				[ typeName: typeName ] ) as GetTemplateDescriptionsResponse
	}

	/**
	 * Returns the list of groups the user is a member of - https://api2.clarizen.com/V2.0/services/data/groupsquery
	 * @param alias property file where you can save configurations
	 * @param model GroupsQueryRequest
	 * @return GroupsQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static GroupsQueryResponse clarizenGroupsQuery ( String alias, GroupsQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'groupsquery' ),
				model ) as GroupsQueryResponse
	}

	/**
	 * Performs life cycle operations - https://api2.clarizen.com/V2.0/services/data/lifecycle
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model LifecycleRequest
	 */
	@CompileStatic
	static void clarizenLifecycle ( String alias, LifecycleRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'lifecycle' ),
				model )
	}

	/**
	 * Returns the current user news feed - https://api2.clarizen.com/V2.0/services/data/newsfeedquery
	 * @param alias property file where you can save configurations
	 * @param model NewsFeedQueryRequest
	 * @return NewsFeedQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static NewsFeedQueryResponse clarizenNewsFeedQuery ( String alias, NewsFeedQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'newsfeedquery' ),
				model ) as NewsFeedQueryResponse
	}

	/**
	 * Read an entity - https://api2.clarizen.com/V2.0/services/data/objects
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param fields String of entity fields
	 * @return ObjectsDataGetResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static Map clarizenObjectsDataGet (
			String alias,
			String typeName,
			String entityId,
			String fields ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.data, 'objects', typeName, entityId ),
				[ fields: [fields]])
	}

	/**
	 * Update an entity - https://api2.clarizen.com/V2.0/services/data/objects
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param objectFieldKey map key
	 * @param objectFieldValue map value
	 */
	@CompileStatic
	@SuppressWarnings([
		'DuplicateStringLiteral',
		'GStringAsMapKey'
	])
	static void clarizenObjectsDataPost (
			String alias,
			String typeName,
			String entityId,
			String objectFieldKey,
			String objectFieldValue ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'objects', typeName, entityId ),
				[ ("${objectFieldKey}".toString()): objectFieldValue ])
	}

	/**
	 * Create an entity - https://api2.clarizen.com/V2.0/services/data/objects
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param objectFieldKey map key
	 * @param objectFieldValue map value
	 * @return ObjectsDataPutResponse
	 */
	@CompileStatic
	@SuppressWarnings([ 'GStringAsMapKey', 'DuplicateStringLiteral' ])
	static ObjectsDataPutResponse clarizenObjectsDataPut (
			String alias,
			String typeName,
			String objectFieldKey,
			String objectFieldValue ) {
		contactClarizenApi(
				alias,
				HttpMethod.put,
				parsePath( ServiceType.data, 'objects', typeName ),
				[ ("${objectFieldKey}".toString()): objectFieldValue ]) as ObjectsDataPutResponse
	}

	/**
	 * Create an entity - https://api2.clarizen.com/V2.0/services/data/objects
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param objectFieldKey map key
	 * @param objectFieldValue map value
	 * @return ObjectsDataPutResponse
	 */
	@CompileStatic
	@SuppressWarnings([ 'GStringAsMapKey', 'DuplicateStringLiteral' ])
	static ObjectsDataPutResponse clarizenObjectsDataPutPreset (
			String alias,
			String typeName,
			String entityId,
			String objectFieldKey,
			String objectFieldValue ) {
		contactClarizenApi(
				alias,
				HttpMethod.put,
				parsePath( ServiceType.data, 'objects', typeName, entityId ),
				[ "$objectFieldKey": objectFieldValue ]) as ObjectsDataPutResponse
	}

	/**
	 * Delete an entity - https://api2.clarizen.com/V2.0/services/data/objects
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static void clarizenObjectsDataDelete ( String alias, String typeName, String entityId ) {
		contactClarizenApi (
				alias,
				HttpMethod.delete,
				parsePath( ServiceType.data, 'objects', typeName, entityId ))
	}

	/**
	 * Executes a CZQL query - https://api2.clarizen.com/V2.0/services/data/query
	 * @param alias property file where you can save configurations
	 * @param model QueryRequest
	 * @return QueryResponse
	 */
	@CompileStatic
	static QueryResponse clarizenQuery ( String alias, QueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'query' ),
				model ) as QueryResponse
	}

	/**
	 * Retrieve the related entities of an object - https://api2.clarizen.com/V2.0/services/data/relationquery
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model RelationQueryRequest
	 * @return RelationQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static RelationQueryResponse clarizenRelationQuery (
			String alias,
			RelationQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'relationquery' ),
				model ) as RelationQueryResponse
	}

	/**
	 * Retrieves the reply feed of a discussion - https://api2.clarizen.com/V2.0/services/data/repliesquery
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model RepliesQueryRequest
	 * @return RepliesQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static RepliesQueryResponse clarizenRepliesQuery (
			String alias,
			RepliesQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'repliesquery' ),
				model ) as RepliesQueryResponse
	}

	/**
	 * Retrieves multiple entities - https://api2.clarizen.com/V2.0/services/data/retrievemultiple
	 * @param alias property file where you can save configurations
	 * @param model RetrieveMultipleRequest
	 * @return RetrieveMultipleResponse
	 */
	@CompileStatic
	static RetrieveMultipleResponse clarizenRetrieveMultiple ( String alias, RetrieveMultipleRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'retrievemultiple' ),
				model ) as RetrieveMultipleResponse
	}

	/**
	 * Performs a text search in a entity type - https://api2.clarizen.com/V2.0/services/data/search
	 * @param alias property file where you can save configurations
	 * @param model SearchRequest
	 * @return SearchResponse
	 */
	@CompileStatic
	static SearchResponse clarizenSearch ( String alias, SearchRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.data, 'search' ),
				modelToMap( model )) as SearchResponse
	}

	/**
	 * Retrieves timesheet according to criterias - https://api2.clarizen.com/V2.0/services/data/timesheetquery
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model TimesheetQueryRequest
	 * @return TimesheetQueryResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static TimesheetQueryResponse clarizenTimeSheetQuery (
			String alias,
			TimesheetQueryRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.data, 'timesheetquery' ),
				model ) as TimesheetQueryResponse
	}

	/**
	 * gets download info about a file attached to a document - https://api2.clarizen.com/V2.0/services/files/download
	 * @param alias property file where you can save configurations
	 * @param documentId id of the document entity
	 * @param redirect if the returned Url is to opened
	 * @return DownloadResponse
	 */
	@CompileStatic
	static void clarizenDownload ( String alias, String documentId, Boolean redirect ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.files, 'download' ),
				[ documentId: documentId, redirect: redirect ])
	}

	/**
	 * Get a URL for uploading files - https://api2.clarizen.com/V2.0/services/files/getuploadurl
	 * @param alias property file where you can save configurations
	 * @return GetUploadUrlResponse
	 */
	@CompileStatic
	static GetUploadUrlResponse clarizenGetUploadurl ( String alias ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.files, 'getuploadurl' )) as GetUploadUrlResponse
	}

	/**
	 * Set (Or Reset) the image of an object in Clarizen - https://api2.clarizen.com/V2.0/services/files/updateimage
	 * @param alias property file where you can save configurations
	 * @param model UpdateImageRequest
	 * @return UpdateImageResponse
	 */
	@CompileStatic
	static UpdateImageResponse clarizenUpdateImage ( String alias, UpdateImageRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.files, 'updateimage' ),
				model ) as UpdateImageResponse
	}

	/**
	 * Upload file to a document in Clarizen - https://api2.clarizen.com/V2.0/services/files/upload
	 * @param alias property file where you can save configurations
	 * @param model UploadRequest
	 */
	@CompileStatic
	static void clarizenUpload ( String alias, UploadRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.files, 'upload' ),
				model )
	}

	/**
	 * info about an Entity Type in Clarizen - https://api2.clarizen.com/V2.0/services/metadata/describeentities
	 * @param alias property file where you can save configurations
	 * @param typeNames the typeName to be described
	 * @return DescribeEntitiesResponse
	 */
	@CompileStatic
	static DescribeEntitiesResponse clarizenDescribeEntities ( String alias, String typeNames ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.metadata, 'describeentities' ),
				[ typeNames: [ typeNames ]]) as DescribeEntitiesResponse
	}

	/**
	 * Describes relation between entities - https://api2.clarizen.com/V2.0/services/metadata/describeentityrelations
	 * @param alias property file where you can save configurations
	 * @param model DescribeEntityRelationsRequest
	 * @return DescribeEntityRelationsResponse
	 */
	@CompileStatic
	static DescribeEntityRelationsResponse clarizenDescribeEntityRelations (
			String alias,
			DescribeEntityRelationsRequest model ) {
			contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.metadata, 'describeentityrelations' ),
				model ) as DescribeEntityRelationsResponse
	}

	/**
	 * Info about the entity types in organization - https://api2.clarizen.com/V2.0/services/metadata/describemetadata
	 * @param alias property file where you can save configurations
	 * @return DescribeMetadataResponse
	 */
	@CompileStatic
	static DescribeMetadataResponse clarizenDescribeMetadata ( String alias ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.metadata, 'describemetadata' )) as DescribeMetadataResponse
	}

	/**
	 * Info about the entity types in organization - https://api2.clarizen.com/V2.0/services/metadata/describemetadata
	 * @param alias property file where you can save configurations
	 * @param typeNames (e.g. WorkItem, Finances)
	 * @return DescribeMetadataResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static DescribeMetadataResponse clarizenDescribeMetadata ( String alias, String typeNames ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.metadata, 'describemetadata' ),
				[ typeNames: [ typeNames ]] ) as DescribeMetadataResponse
	}

	/**
	 * Info about the entity types in organization - https://api2.clarizen.com/V2.0/services/metadata/describemetadata
	 * @param alias property file where you can save configurations
	 * @param typeNames (e.g. WorkItem, Finances)
	 * @param flags Field of the typeNames
	 * @return DescribeMetadataResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static DescribeMetadataResponse clarizenDescribeMetadata ( String alias, String typeNames, String flags ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.metadata, 'describemetadata' ),
				[ typeNames: [ typeNames ], flags: [ flags] ] ) as DescribeMetadataResponse
	}

	/**
	 * Values of a system settings - https://api2.clarizen.com/V2.0/services/metadata/getsystemsettingsvalues
	 * @param alias property file where you can save configurations
	 * @param settings Clarizen settings name
	 * @return GetSystemSettingsValuesResponse
	 */
	@CompileStatic
	static GetSystemSettingsValuesResponse clarizenGetSystemSettingsValues ( String alias, String settings ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.metadata, 'getsystemsettingsvalues' ),
				[ settings: [ settings ]] ) as GetSystemSettingsValuesResponse
	}

	/**
	 * List of entity types available in organization - https://api2.clarizen.com/V2.0/services/metadata/listentities
	 * @param alias property file where you can save configurations
	 * @return ListEntitiesResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static ListEntitiesResponse clarizenListEntites ( String alias ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.metadata, 'listentities' )) as ListEntitiesResponse
	}

	/**
	 * create a workflow rule - https://api2.clarizen.com/V2.0/services/metadata/objects
	 * @param alias property file where you can save configurations
	 * @param model ObjectsMetadataRequest
	 * @param typeName Entity typename
	 * @return ObjectsMetadataResponse
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static ObjectsMetadataResponse clarizenObjectsMetadataPut (
		String alias,
		ObjectsMetadataRequest model,
		String typeName ) {
		contactClarizenApi(
				alias,
				HttpMethod.put,
				parsePath( ServiceType.metadata, 'objects', typeName ),
				model ) as ObjectsMetadataResponse
	}

	/**
	 * delete a workflow rule - https://api2.clarizen.com/V2.0/services/metadata/objects
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static void clarizenObjectsMetadataDelete ( String alias, String typeName, String entityId ) {
		contactClarizenApi(
				alias,
				HttpMethod.delete,
				parsePath( ServiceType.metadata, 'objects', typeName, entityId ))
	}

	/**
	 * delete a workflow rule - https://api2.clarizen.com/V2.0/services/metadata/objects
	 * @param alias property file where you can save configurations
	 * @param fieldName name of the settings
	 * @param value Basic element of Clarizen
	 */
	@CompileStatic
	@SuppressWarnings( 'DuplicateStringLiteral' )
	static void clarizenSetSystemSettingsValue ( String alias, String fieldName, String value ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.metadata, 'setsystemsettingsvalues' ),
				[settings: [ [fieldName: fieldName, value: value] ]])
	}

	/**
	 * Get info about the application - https://api2.clarizen.com/V2.0/services/applications/getapplicationstatus
	 * @param alias property file where you can save configurations
	 * @param applicationId ID of the application from marketplace
	 * @return GetApplicationStatusResponse
	 */
	@CompileStatic
	static GetApplicationStatusResponse clarizenGetApplicationStatus ( String alias, String applicationId ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.applications, 'getapplicationstatus' ),
				[ applicationId: applicationId ] ) as GetApplicationStatusResponse
	}

	/**
	 * get status of the application request - https://api2.clarizen.com/V2.0/services/applications/installapplication
	 * @param alias property file where you can save configurations
	 * @param model InstallApplicationRequest
	 */
	@CompileStatic
	static void clarizenInstallApplication ( String alias, InstallApplicationRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.applications, 'installapplication' ),
				model )
	}

	/**
	 * Converts an API session to a web application session - https://api2.clarizen.com/V2.0/services/bulk/execute
	 * @param alias property file where you can save configurations
	 * @param model ExecuteRequest
	 * @return ExecuteResponse
	 */
	@CompileStatic
	static ExecuteResponse clarizenExecute ( String alias, ExecuteRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.bulk, 'execute' ),
				model ) as ExecuteResponse
	}

	/**
	 * Converts an API session to a web application session  - https://api2.clarizen.com/V2.0/services/utils/applogin
	 * @param alias property file where you can save configurations
	 * @return AppLoginResponse
	 */
	@CompileStatic
	static AppLoginResponse clarizenAppLogin ( String alias ) {
		contactClarizenApi(
				alias,
				HttpMethod.get,
				parsePath( ServiceType.utils, 'applogin' )) as AppLoginResponse
	}

	/**
	 * Send an email and attach it to an object in Clarizen - https://api2.clarizen.com/V2.0/services/utils/sendemail
	 * @param alias property file where you can save configurations
	 * @param typeName Entity Type
	 * @param entityId Basic element of Clarizen
	 * @param model SendEmailRequest
	 */
	@CompileStatic
	static void clarizenSendEmail ( String alias, SendEmailRequest model ) {
		contactClarizenApi(
				alias,
				HttpMethod.post,
				parsePath( ServiceType.utils, 'sendemail' ),
				model )
	}
}
