package io.toro.clarizen.exception

/**
 * Created by daren on 22/06/15.
 */
class ClarizenException extends Exception {

    ClarizenException( String message ) {
        super( message )
    }

    ClarizenException( Throwable t ) {
        super( t )
    }
}
