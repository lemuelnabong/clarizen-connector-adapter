package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings('PropertyName')
@ToString( includeNames = true )
class ConditionExpression extends Condition {
	/**
	 * Condition type
	 */
	String _type

	/**
	 * Expression
	 */
	Expression leftExpression

	/**
	 * Expression
	 */
	Expression rightExpression

	/**
	 *
	 */
	String operator
}
