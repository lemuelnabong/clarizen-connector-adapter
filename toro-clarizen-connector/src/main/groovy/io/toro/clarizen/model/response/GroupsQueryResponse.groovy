package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.Entity
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GroupsQueryResponse {
	/**
	 * Array of Entity
	 */
	Entity[] items = new Entity()

	/**
	 * Paging information for a query
	 */
	Paging paging
}
