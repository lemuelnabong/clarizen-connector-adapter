package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CreateFromTemplateRequest {
	/**
	 * Name of the template to use
	 */
	String templateName

	/**
	 * EntityId
	 */
	String[] parentId

	/**
	 * Entity
	 */
	Map entity
}
