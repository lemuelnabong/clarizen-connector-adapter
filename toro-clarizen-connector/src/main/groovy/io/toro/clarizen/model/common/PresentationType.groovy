package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum PresentationType {
  Text,
  Numeric,
  Date,
  Checkbox,
  TextArea,
  Currency,
  Duration,
  ReferenceToObject,
  PickList,
  Url,
  Percent,
  RichText,
  MultiPickList,
  Other
}
