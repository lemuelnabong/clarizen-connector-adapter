package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class AggregateQuery extends Query {
	/**
	 * The main entity type to query
	 */
	String typeName

	/**
	 * A list of field names to group results by
	 */
	String[] groupBy

	/**
	 * Optionaly order the result
	 */
	OrderBy[] orders

	/**
	 * The query criteria
	 */
	Condition where = new And()

	/**
	 * List of aggregations to perform
	 */
	FieldAggregation[] aggregations
}
