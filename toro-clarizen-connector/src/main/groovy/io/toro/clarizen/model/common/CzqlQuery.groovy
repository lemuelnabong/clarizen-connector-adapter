package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CzqlQuery extends Query {
	/**
	 * The CZQL Query to perform
	 */
	String q

	/**
	 * Values for bind parameters
	 */
	Parameters parameters
}
