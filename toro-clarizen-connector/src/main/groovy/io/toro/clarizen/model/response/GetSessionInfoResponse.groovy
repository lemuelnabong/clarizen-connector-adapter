package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.FieldValue
import io.toro.clarizen.model.common.LicenseType

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetSessionInfoResponse {
	/**
	 * Array of FieldValue
	 */
	FieldValue[] customInfo

	/**
	 * A unique ID representing the current session
	 */
	String sessionId

	/**
	 * The unique ID of the current user
	 */
	String userId

	/**
	 * The unique ID of the current organization
	 */
	String organizationId

	/**
	 * Indicates which license the current user is assigned
	 */
	LicenseType licenseType

	/**
	 * Time in the server when request is invoked
	 */
	String serverTime
}
