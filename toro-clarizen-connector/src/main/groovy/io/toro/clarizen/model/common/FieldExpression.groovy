package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FieldExpression extends Expression {
	/**
	 * The name of the field this expression represents
	 */
	String fieldName

	/**
	 * Value of the field
	 */
	String value
}
