package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CreateDiscussionRequest {
	/**
	 * Array of Entity
	 */
	String[] relatedEntities

	/**
	 * Array of Entity
	 */
	String[] notify

	/**
	 * Array of Entity
	 */
	String[] topics

	/**
	 * Entity entity
	 */
	Map entity
}
