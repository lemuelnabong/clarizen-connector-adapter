package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum Operator {
  Equal,
  NotEqual,
  LessThan,
  GreaterThan,
  LessThanOrEqual,
  GreaterThanOrEqual,
  BeginsWith,
  EndsWith,
  Contains,
  DoesNotContain,
  In,
  Like
}
