package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class EntityFeedQueryRequest {
	/**
	 * Entity
	 */
    String entityId

    /**
	 * List of Fields the query should return
	 */
	String[] fields

	/**
	 * Array of strings or comma separated list of strings
	 */
	String[] feedItemOptions

	/**
	 * Paging
	 */
	Paging paging
}
