package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class QueryExpression extends Expression {
	/**
	 * Query
	 */
	Query fieldName = new EntityQuery()
}
