package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Relation  {
	/**
	 * Relation name
	 */
	String name

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * The query criteria
	 */
	Condition where = new And()

	/**
	 * Optionaly order the result
	 */
	OrderBy orders

	/**
	 * The query result of link objects
	 */
	Boolean fromLink
}
