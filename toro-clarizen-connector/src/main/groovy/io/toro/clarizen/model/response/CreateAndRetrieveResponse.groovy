package io.toro.clarizen.model.response

import io.toro.clarizen.model.common.Entity
import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CreateAndRetrieveResponse {
	/**
	 * Entity
	 */
	Entity entity
}
