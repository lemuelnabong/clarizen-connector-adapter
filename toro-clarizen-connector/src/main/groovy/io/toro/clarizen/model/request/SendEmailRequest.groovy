package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.Recipient

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class SendEmailRequest {

	/**
	 * String
	 */
	String subject

	/**
	 * String
	 */
	String body

	/**
	 * Array of Recipients
	 */
    Recipient[] recipients

    /**
	 * EntityId
	 */
	String[] relatedEntity

	/**
	 * AccessType
	 */
	String accessType
}
