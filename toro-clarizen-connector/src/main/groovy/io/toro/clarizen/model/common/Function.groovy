package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum Function {
  Count,
  Max,
  Min,
  Sum,
  Avg
}
