package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class DescribeEntitiesRequest {
	/**
	 * The types of entities to describe
	 */
	 String[] typeNames
}
