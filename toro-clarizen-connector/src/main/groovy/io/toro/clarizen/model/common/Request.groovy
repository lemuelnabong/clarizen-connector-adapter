package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Request   {

	/**
	 * String
	 */
    String url

	/**
	 * The HttpMethod
	 */
    String method

	/**
	 * Request body
	 */
    String body
}
