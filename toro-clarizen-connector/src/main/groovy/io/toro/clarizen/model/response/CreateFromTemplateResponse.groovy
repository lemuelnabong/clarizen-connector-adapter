package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CreateFromTemplateResponse {
	/**
	 * entity ID
	 */
	String id
}
