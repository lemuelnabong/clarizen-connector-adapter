package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.OrderBy
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FindUserQueryRequest {
	/**
	 * String
	 */
	String firstName

	/**
	 * String
	 */
	String lastName

	/**
	 * String
	 */
	String eMail

	/**
	 * Boolean
	 */
	Boolean fuzzySearchUserName

	/**
	 * Boolean
	 */
	Boolean includeSuspendedUsers

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * Optionaly order the result
	 */
    OrderBy[] orders

    /**
	 * Paging setting for the query
	 */
	Paging paging
}
