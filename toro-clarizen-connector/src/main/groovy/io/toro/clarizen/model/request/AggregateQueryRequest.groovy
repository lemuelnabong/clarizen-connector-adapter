package io.toro.clarizen.model.request

import io.toro.clarizen.model.common.Condition
import io.toro.clarizen.model.common.FieldAggregation
import io.toro.clarizen.model.common.Paging
import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class AggregateQueryRequest {
	/**
	 * The main entity type to query
	 */
    String typeName

    /**
	 * A list of field names to group results by
	 */
    String[] groupBy

    /**
	 * Optionaly order the result
	 */
    OrderBy[] orders

    /**
	 * The query criteria
	 */
    Condition where

    /**
	 * List of aggregations to perform
	 */
    FieldAggregation[] aggregations

    /**
	 * setting for the query
	 */
    Paging paging
}
