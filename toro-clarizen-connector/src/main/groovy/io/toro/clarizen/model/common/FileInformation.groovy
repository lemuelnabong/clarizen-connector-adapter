package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FileInformation {
	/**
	 * storage - enum
	 */
	 String storage

	/**
	 * String url
	 */
	 String url

	/**
	 * String fileName
	 */
	 String fileName

	/**
	 * String subType
	 */
	 String subType

	/**
	 * String extendInfo
	 */
	 String extendedInfo
}
