package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.LoginOptions

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class AccessCredentialRequest {
	/**
	 * The user name to authenticate with
	 */
	String userName

	/**
	 * The password to authenticate with
	 */
    String password

    /**
	 * Additional information required during the login process
	 */
    LoginOptions loginOptions
}
