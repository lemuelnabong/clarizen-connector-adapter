package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.EntityDescription

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class DescribeMetadataResponse {
	/**
	 * Array of EntityDescription
	 */
	EntityDescription[] entityDescriptions
}
