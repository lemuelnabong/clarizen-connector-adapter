package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum HttpMethod {
  POST,
  post,
  DELETE,
  delete,
  GET,
  get,
  PUT,
  put,
  HEAD,
  head,
  TRACE,
  trace,
  CONNECT,
  connect,
  PATCH,
  patch
}
