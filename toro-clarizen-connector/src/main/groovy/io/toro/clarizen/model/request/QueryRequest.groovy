package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.Parameters
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class QueryRequest {
	/**
	 * The CZQL Query to perform
	 */
	String q

	/**
	 * Values for bind parameters
	 */
	Parameters parameters

	/**
	 * Paging setting for the query
	 */
	Paging paging
}
