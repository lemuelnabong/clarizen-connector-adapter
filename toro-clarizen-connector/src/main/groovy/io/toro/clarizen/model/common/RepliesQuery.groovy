package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class RepliesQuery extends Query {
	/**
	 * Id of the discussion post
	 */
	String postId

	/**
	 * list of Fields the query should return
	 */
	String[] fields

	/**
	 * Array of strings or comma separated list of strings
	 */
	String[] feedItemOptions
}
