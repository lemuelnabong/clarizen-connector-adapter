package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.ReplyFeedItem
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class RepliesQueryResponse {
	/**
	 * ReplyFeedItem
	 */
	ReplyFeedItem[] items

	/**
	 * Paging setting for the query
	 */
	Paging paging
}
