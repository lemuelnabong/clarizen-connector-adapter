package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.NewsFeedMode
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class NewsFeedQueryRequest {
	/**
	 * Mode of the news feed query: Following or All
	 */
	NewsFeedMode mode

	/**
	 * list of Fields the query should return
	 */
	String[] fields

	/**
	 * Array of strings or comma separated list of strings
	 */
	String[] feedItemOptions

	/**
	 * paging setting for the query
	 */
	Paging paging
}
