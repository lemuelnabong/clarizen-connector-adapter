package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class TimesheetQuery extends Query {
	/**
	 * Represents the unique Id of an entity in Clarizen
	 */
	String projectId

	/**
	 * Represents the unique Id of an entity in Clarizen
	 */
	String customerId

	/**
	 * Boolean
	 */
	Boolean iAmTheApprover

	/**
	 * Array that represents the unique Id of an entity in Clarizen
	 */
	String[] workitems

	/**
	 * TimesheetState
	 */
	TimesheetState timesheetstate

	/**
	 * The main entity type to query
	 */
	String typeName

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * Optionaly order the result
	 */
	OrderBy[] orders

	/**
	 * The query criteria
	 */
	Condition where = new And()

	/**
	 * The query relations
	 */
	Relation[] relations

	/**
	 * If set to true, the query is performed on Deleted entities
	 */
	Boolean deleted
}

