package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Action {

	/**
	 * url The actual url
	 */
	String url

	/**
	 * The HTTP method
	 */
	HttpMethod method

	/**
	 * The multiline formula that represents a list of the http headers in the following format
	 */
	String headers

	/**
	 * body The formula that represents a body of the http request
	 */
	String body
}
