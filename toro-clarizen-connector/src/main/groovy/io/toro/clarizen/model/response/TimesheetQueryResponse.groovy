package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.Entity
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class TimesheetQueryResponse {
	/**
	 * Array of entities returned from this query
	 */
	Entity[] entities

	/**
	 * Paging information returned from this query
	 */
	Paging paging
}
