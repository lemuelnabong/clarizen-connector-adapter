package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetSystemSettingsValuesResponse {
	/**
	 * Array of objects representing the values of each system setting
	 */
	String values
}
