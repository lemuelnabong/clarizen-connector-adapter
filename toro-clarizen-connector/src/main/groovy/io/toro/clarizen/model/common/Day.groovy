package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum Day {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday
}
