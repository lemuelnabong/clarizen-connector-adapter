package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class And extends Condition {
	/**
	 * ConditionExpression List of conditions to AND
	 */
	ConditionExpression[] and
}
