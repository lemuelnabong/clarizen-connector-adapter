package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class InstallApplicationRequest {
	/**
	 * String
	 */
	String applicationId

	/**
	 * Boolean
	 */
	Boolean autoEnable
}
