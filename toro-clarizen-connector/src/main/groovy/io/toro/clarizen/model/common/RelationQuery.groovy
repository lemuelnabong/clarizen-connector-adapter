package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class RelationQuery extends Query {
	/**
	 * The main entity
	 */
	String entityId

	/**
	 * Relation name to retrieve
	 */
	String relationName

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * Optionaly order the result
	 */
	OrderBy[] orders

	/**
	 * The query criteria
	 */
	Condition where = new And()

	/**
	 * The query relations
	 */
	Relation[] relations

	/**
	 * The query result of link objects
	 */
	Boolean fromLink
}
