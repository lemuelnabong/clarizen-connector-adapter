package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.Response

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class ExecuteResponse {
	/**
	 * Array of Response objects representing API responses for each API call
	 */
	Response[] responses
}
