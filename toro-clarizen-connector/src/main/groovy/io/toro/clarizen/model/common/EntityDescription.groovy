package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class EntityDescription {
	/**
	 * String
	 */
	 String typeName

	/**
	 * String[]
	 */
	 String[] validStates

	/**
	 * String
	 */
	 String label

	/**
	 * String
	 */
	 String labelPlural

	/**
	 * String
	 */
	 String parentEntity

	/**
	 * String
	 */
	 String displayField

	/**
	 * Boolean
	 */
	 Boolean disabled

	 /**
	  * PossibleLinks
	  */
	 PossibleLinks[] possibleLinks

	 /**
	  * determine if a model is customizable
	  */
	 boolean customizable

	 /**
	  * Pickups
	  */
	 Pickup[] pickups
}
