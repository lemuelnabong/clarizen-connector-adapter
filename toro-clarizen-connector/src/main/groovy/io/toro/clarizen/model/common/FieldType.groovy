package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum FieldType {
  Boolean,
  String,
  Integer,
  Long,
  Double,
  DateTime,
  Date,
  Entity,
  Duration,
  Money,
  MultiPickList,
  Binary
}
