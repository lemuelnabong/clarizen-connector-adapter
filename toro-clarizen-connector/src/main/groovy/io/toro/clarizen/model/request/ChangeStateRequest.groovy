package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class ChangeStateRequest {
	/**
	 * List of objects to perform the operation on
	 */
	String[] ids

	/**
	 * The new state
	 */
	String state
}
