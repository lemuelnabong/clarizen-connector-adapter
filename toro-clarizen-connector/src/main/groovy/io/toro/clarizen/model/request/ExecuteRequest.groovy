package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.Request

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class ExecuteRequest {
	/**
	 * Array of Request objects representing individual API calls
	 */
    Request[] requests

    /**
	 * Boolean
	 */
	Boolean transactional
}
