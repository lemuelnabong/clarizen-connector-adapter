package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Response {
	/**
	 * Status code (200 for OK, 500 for error)
	 */
	Integer statusCode

	/**
	 * Response body as a JSON string
	 */
	String body
}
