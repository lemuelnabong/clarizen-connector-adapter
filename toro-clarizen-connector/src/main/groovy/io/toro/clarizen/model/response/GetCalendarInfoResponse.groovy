package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.Day
import io.toro.clarizen.model.common.DayInformation

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetCalendarInfoResponse {
	/**
	 * Indicates the day which is considered the first day of the week
	 */
	Day weekStartOn

	/**
	 * Provides information about each day of the week. The first day in the array is Sunday
	 */
	DayInformation[] weekDayInformation

	/**
	 * Provides working day information about a calendar day
	 */
	DayInformation defaultWorkingDay

	/**
	 * Working days per month
	 */
	Integer workingDaysPerMonth
}
