package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FieldAggregation {
	/**
	 * Function enum - Type of aggregate function to perform
	 */
	Function function

	/**
	 * Name of a field to perform this function on
	 */
	String fieldName

	/**
	 * (Optional) A name that will represent the result of this function
	 */
	String alias
}
