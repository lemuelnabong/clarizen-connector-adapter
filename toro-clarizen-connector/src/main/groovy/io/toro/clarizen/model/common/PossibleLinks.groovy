package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class PossibleLinks {
	/**
	 * relation origin field
	 */
	String fromField

	/**
	 * relation origin entity
	 */
	String fromEntity

	/**
	 * relation destination field
	 */
	String toField

	/**
	 * relation destination entity
	 */
	String toEntity
}
