package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class QueryResponse {
	/**
	 * Array of entities returned from this query
	 */
	HashMap<String, Object>[] entities

	/**
	 * Paging information returned from this query
	 */
	Paging paging
}
