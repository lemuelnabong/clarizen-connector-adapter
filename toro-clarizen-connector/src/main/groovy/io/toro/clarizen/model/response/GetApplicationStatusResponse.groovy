package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetApplicationStatusResponse {
	/**
	 * Check wether the application is installed
	 */
	Boolean installed

	/**
	 * Application version
	 */
	String version
}
