package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class AppLoginResponse {
	/**
	 * A Url that can be used to enter the web application without requireing credentials
	 */
	String url
}
