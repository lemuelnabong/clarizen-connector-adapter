package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.PostFeedItem
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class EntityFeedQueryResponse {
	/**
	 * Array of PostFeedItem
	 */
    PostFeedItem[] items

    /**
	 * Paging
	 */
    Paging paging
}
