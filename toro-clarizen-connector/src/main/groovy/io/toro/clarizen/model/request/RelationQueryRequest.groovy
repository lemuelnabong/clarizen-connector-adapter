package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.OrderBy
import io.toro.clarizen.model.common.Relation
import io.toro.clarizen.model.common.Paging
import io.toro.clarizen.model.common.Condition

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class RelationQueryRequest  {
	/**
	 * The main entity
	 */
	String entityId

	/**
	 * relation name to retrieve
	 */
	String relationName

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * orders Optionally the result
	 */
    OrderBy[] orders

    /**
	 * The query criteria
	 */
	Condition where

	/**
	 * The query relations
	 */
    Relation[] relations

    /**
	 * The query result of link objects
	 */
	Boolean fromLink

	/**
	 * Paging setting for the query
	 */
	Paging paging
}
