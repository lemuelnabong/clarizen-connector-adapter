package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class EntityFeedQuery extends Query {
	/**
	 * Represents the unique Id of an entity in Clarizen
	 */
	String entityId

	/**
	 * list of Fields the query should return
	 */
	String[] fields

	/**
	 * Array of strings or comma separated list of strings
	 */
	String[] feedItemOptions
}
