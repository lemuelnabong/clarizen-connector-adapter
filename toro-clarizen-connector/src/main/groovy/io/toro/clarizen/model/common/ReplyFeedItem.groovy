package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class ReplyFeedItem {
	/**
	 *
	 */
    Entity[] message

    /**
	 *
	 */
    Boolean likedByMe

    /**
	 *
	 */
    Entity[] relatedEntities

    /**
	 *
	 */
    Entity[] notify

    /**
	 *
	 */
    Entity topics

    /**
	 *
	 */
    String bodyMarkup

    /**
	 *
	 */
    String summary
}
