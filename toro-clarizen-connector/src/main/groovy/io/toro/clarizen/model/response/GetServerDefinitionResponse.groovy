package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetServerDefinitionResponse {
	/**
	 * The actual API url to use for subsequent calls
	 */
	String serverLocation

	/**
	 * The url of the user application
	 */
    String appLocation
}
