package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum LicenseType {
    Full,
    Limited,
    Email,
    None,
    TeamMember,
    Social
}
