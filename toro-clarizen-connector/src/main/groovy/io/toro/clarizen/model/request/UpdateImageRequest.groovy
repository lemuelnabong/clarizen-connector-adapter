package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class UpdateImageRequest {
	/**
	 * Id of an entity to attach to
	 */
	String entityId

	/**
	 * provide the Url you received in a previous request to getUploadUrl
	 */
	String uploadUrl

	/**
	 * Revert image to default icon
	 */
	Boolean reset
}
