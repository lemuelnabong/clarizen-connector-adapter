package io.toro.clarizen.model.response

import groovy.transform.ToString
import io.toro.clarizen.model.common.FileInformation

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class DownloadResponse {
	/**
	 * FileInformation
	 */
	FileInformation fileInformation
}
