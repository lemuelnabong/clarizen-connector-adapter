package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class LoginOptions {
	/**
	 * (Optional) If you are a certified partner, please provide the partner id
	 */
	String partnerId

	/**
	 * A string representing your application
	 */
	String applicationId
}
