package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class DescribeMetadataRequest {
	/**
	 * The types of entities to describe
	 */
	String[] typeNames

	/**
	 * Optional) Flags that define which information to return
	 */
	String[] flags
}
