package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class EntityQuery extends Query {
	/**
	 * Specify the type of the query
	 */
	@SuppressWarnings( 'PropertyName' )
	String _type

	/**
	 * The main entity type to query
	 */
	String typeName

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * Optionaly order the result
	 */
	OrderBy[] orders

	/**
	 * The query criteria
	 */
	Condition where

	/**
	 * The query relations
	 */
	Relation[] relations

	/**
	 * If set to true, the query is performed on Deleted entities
	 */
	Boolean deleted
}
