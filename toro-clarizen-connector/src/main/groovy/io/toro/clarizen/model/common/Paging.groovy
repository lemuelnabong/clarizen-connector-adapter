package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Paging {
	/**
	 * The record number to start retrieve from
	 */
	Integer from

	/**
	 * Number of records to retrieve
	 */
	Integer limit

	/**
	 * When a query results is returned, indicates whether there are more records to fetch for this query
	 */
	Boolean hasMore
}
