package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class PostFeedItem {
	/**
	 * Last two replies in this post
	 */
    ReplyFeedItem[] latestReplies

    /**
	 * A flag indicating whether this message is pinned\highlighted
	 */
    Boolean pinned

    /**
	 * The discussion message entity
	 */
    Entity message

    /**
	 * Boolean flag which indicates whether the current user likes this message
	 */
    Boolean likedByMe

    /**
	 * related items mentioned in this message
	 */
    Entity[] relatedEntities

    /**
	 * users or groups notified in this message
	 */
    Entity[] notify

    /**
	 * topics in this message
	 */
    Entity[] topics

    /**
	 * String
	 */
    String bodyMarkup

    /**
	 * String
	 */
    String summary
}
