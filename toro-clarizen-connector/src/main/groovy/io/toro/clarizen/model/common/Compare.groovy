package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Compare extends Condition {
	/**
	 * Expression
	 */
	Expression leftExpression = new FieldExpression()

	/**
	 * Expression
	 */
	Expression rightExpression = new FieldExpression()

	/**
	 * Operator
	 */
	Operator operator
}
