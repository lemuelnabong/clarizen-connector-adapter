package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class LifecycleRequest {
	/**
	 * A list of objects to perform the operation on
	 */
	String[] ids

	/**
	 * The operation to perform ('Activate', 'Cancel' etc.)
	 */
	String operation
}
