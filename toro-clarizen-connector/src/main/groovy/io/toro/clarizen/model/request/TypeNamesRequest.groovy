package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class TypeNamesRequest {
	/**
	 * Actual URL of the server location
	 */
	String serverLocation

	/**
	 * Application location within clarizen
	 */
	String appLocation
}
