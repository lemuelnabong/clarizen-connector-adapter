package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.OrderBy
import io.toro.clarizen.model.common.Condition
import io.toro.clarizen.model.common.Relation
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class TimesheetQueryRequest  {
	/**
	 * Represents the unique Id of an entity in Clarizen
	 */
	String[] projectId

	/**
	 * Represents the unique Id of an entity in Clarizen
	 */
	String[] customerId

	/**
	 * Boolean
	 */
	Boolean iAmTheApprover

	/**
	 * Array that represents the unique Id of an entity in Clarizen
	 */
	String[] workitems

	/**
	 * TimesheetState
	 */
	String timesheetState

	/**
	 * The main entity type to query
	 */
	String typeName

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * Optionaly order the result
	 */
    OrderBy[] orders

    /**
	 * The query criteria
	 */
	Condition where

	/**
	 * The query relations
	 */
    Relation[] relations

    /**
	 * If set to true, the query is performed on Deleted entities
	 */
	Boolean deleted

	/**
	 * Paging setting for the query
	 */
	Paging paging
}
