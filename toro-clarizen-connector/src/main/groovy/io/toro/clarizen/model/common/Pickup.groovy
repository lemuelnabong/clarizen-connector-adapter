package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Pickup {
	/**
	 * actual value of the pickup
	 */
	String value

	/**
	 * user displayed value of the pickup
	 */
	String displayValue

	/**
	 * describe the pickup
	 */
	String description

	/**
	 * color
	 */
	String color

	/**
	 * actual url of the image
	 */
	String imageUrl
}
