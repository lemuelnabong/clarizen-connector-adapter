package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.FileInformation

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class UploadRequest {

	/**
	 * Id of an entity to attach to
	 */
	String documentId

	/**
	 * Additional information about the file
	 */
	FileInformation fileInformation

	/**
	 * Provide the Url you received in a previous request to getUploadUrl
	 */
	String uploadUrl
}
