package io.toro.clarizen.model.common

/**
 * Created by Lem on 07/06/15
 */
@SuppressWarnings([ 'FieldName', 'PropertyName' ])
enum ServiceType {
	authentication,
	data,
	applications,
	bulk,
	metadata,
	utils,
	files
}
