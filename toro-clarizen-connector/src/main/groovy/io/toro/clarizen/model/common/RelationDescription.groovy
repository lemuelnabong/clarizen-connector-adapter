package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class RelationDescription {
	/**
	 * String
	 */
	String includeNames

	/**
	 * String
	 */
	String label

	/**
	 * String
	 */
	String roleLabel

	/**
	 * Boolean
	 */
	Boolean readOnly

	/**
	 * String
	 */
	String linkTypeName

	/**
	 * String
	 */
	String relatedTypeName

	/**
	 * String
	 */
	String sourceFieldName
}
