package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.Action

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class ObjectsMetadataRequest {
	/**
	 * The entity where the workflow rule is created
	 */
	String forType

	/**
	 * The name of the workflow rule
	 */
	String name

	/**
	 * The description of the workflow rule
	 */
	String description

	/**
	 * The type of event that trigger the workflow rule
	 */
	String triggerType

	/**
	 * The condition for the workflow rule triggering
	 */
	String criteria

	/**
	 * The workflow action
	 */
	Action action
}
