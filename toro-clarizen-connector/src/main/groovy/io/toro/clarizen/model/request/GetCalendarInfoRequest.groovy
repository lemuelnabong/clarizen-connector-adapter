package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetCalendarInfoRequest {
	/**
	 * (Optional) If UserId is specified, the result will contain infor about the calendar
	 */
	String userId
}
