package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class DayInformation {
	/**
	 * Indicates whether this day is a working day
	 */
	Boolean isWorkingDay

	/**
	 * Total number of working hour in this day. Only relevant if is true
	 */
	Double totalWorkingHours

	/**
	 * A number between 0 and 24 representing the first working hour of the day
	 */
	Double startHour

	/**
	 * A number between 0 and 24 representing the end of the working day
	 */
	Double endHour
}
