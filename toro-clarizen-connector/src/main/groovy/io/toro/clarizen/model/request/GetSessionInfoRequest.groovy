package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.LoginOptions

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetSessionInfoRequest {
	/**
	 * LoginOptions
	 */
	LoginOptions options
}
