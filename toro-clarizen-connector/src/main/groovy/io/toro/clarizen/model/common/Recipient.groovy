package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Recipient  {
	/**
	 * Kind of recipient
	 */
	String recipientType

	/**
	 * The username
	 */
	String user

	/**
	 * Email of the recipient
	 */
	String eMail
}
