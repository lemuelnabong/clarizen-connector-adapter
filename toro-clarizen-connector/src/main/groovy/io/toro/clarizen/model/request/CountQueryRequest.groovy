package io.toro.clarizen.model.request

import io.toro.clarizen.model.common.Query
import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CountQueryRequest  {
	/**
	 * Query
	 */
	Query query
}
