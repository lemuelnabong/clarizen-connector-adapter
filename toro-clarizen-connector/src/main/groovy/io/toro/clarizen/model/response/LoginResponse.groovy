package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class LoginResponse {
	/**
	 * A unique ID representing the current session
	 */
	String sessionId

	/**
	 * The unique ID of the current user
	 */
    String userId

    /**
	 * The unique ID of the current organization
	 */
    String organizationId

    /**
      * The unique ID of the current organization
      */
    String serverTime

    /**
	 * Indicates which license the current user is assigned
	 */
    String licenseType
}
