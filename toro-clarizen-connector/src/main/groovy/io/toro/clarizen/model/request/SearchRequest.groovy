package io.toro.clarizen.model.request

import groovy.transform.ToString
import io.toro.clarizen.model.common.Paging

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class SearchRequest {
	/**
	 * (Optional) The Entity Type to search. If omitted, search on all types
	 */
	String typeName

	/**
	 * The search query to perform
	 */
	String q

	/**
	 * The list of fields to return. Only valid when specifying a TypeName
	 */
	String[] fields

	/**
	 * Paging setting for the query
	 */
    Paging paging
}
