package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FieldDescription {
	/**
	 * String
	 */
	 String name

	/**
	 * FieldType enum
	 */
	 FieldType type

	/**
	 * PresentationType enum
	 */
	 PresentationType presentationType

	/**
	 * String
	 */
	 String label

	/**
	 * String
	 */
	 String defaultValue

	/**
	 * Boolean
	 */
	 Boolean system

	/**
	 * Boolean
	 */
	 Boolean calculated

	/**
	 * Boolean
	 */
	 Boolean nullable

	/**
	 * Boolean
	 */
	 Boolean createOnly

	/**
	 * Boolean
	 */
	 Boolean updateable

	/**
	 * Boolean
	 */
	 Boolean internal

	/**
	 * Boolean
	 */
	 Boolean custom

	/**
	 * Boolean
	 */
	 Boolean visible

	/**
	 * Boolean
	 */
	 Integer decimalPlaces

	/**
	 * Boolean
	 */
	 Boolean filterable

	/**
	 * Boolean
	 */
	 Boolean sortable

	/**
	 * integer
	 */
	 Integer maxLength

	/**
	 * String[]
	 */
	 String[] flags
}
