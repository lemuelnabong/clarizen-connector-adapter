package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class CzqlCondition extends Condition {
	/**
	 * The condition text
	 */
	String text

	/**
	 * Values for bind parameters
	 */
	Parameters parameters
}
