package io.toro.clarizen.model.response

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class GetTemplateDescriptionsResponse {
	/**
	 * Array of strings or comma separated list of strings
	 */
	String[] templates
}
