package io.toro.clarizen.model.request

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class RetrieveMultipleRequest {
	/**
	 * Array of strings or comma separated list of strings
	 */
	String[] fields

	/**
	 * Represents the unique Id of an entity in Clarizen
	 */
	String[] ids
}
