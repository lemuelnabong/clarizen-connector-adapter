package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FieldExpressionName extends Expression {
	/**
	 * The name of the field this expression represents
	 */
	String fieldName
}
