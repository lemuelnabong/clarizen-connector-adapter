package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class FindUserQuery extends Query {
	/**
	 * String
	 */
	String firstName

	/**
	 * String
	 */
	String lastName

	/**
	 * String
	 */
	String eMail

	/**
	 * Boolean
	 */
	Boolean fuzzySearchUserName

	/**
	 * Boolean
	 */
	Boolean includeSuspendedUsers

	/**
	 * A list of field names to retrieve
	 */
	String[] fields

	/**
	 * Optionaly order the result
	 */
	OrderBy[] orders
}
