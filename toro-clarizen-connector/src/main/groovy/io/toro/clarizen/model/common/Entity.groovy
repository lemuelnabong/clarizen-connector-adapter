package io.toro.clarizen.model.common

import groovy.transform.ToString

/**
 * Created by Lem on 07/06/15
 */
@ToString( includeNames = true )
class Entity {
	/**
	 * Entity ID
	 */
	String id

	/**
	 * Object fields in JSON format
	 */
	Map objectFields
}
