/*
 * ordered by: service type, alphabetical
 * based on : https://api.clarizen.com/v2.0/services/
 * 
 */
import com.toro.licensing.LicenseManagerProperties
import com.toro.licensing.provider.ProductKeyProvider
import com.toro.licensing.provider.ToroPublicKeyPasswordProvider
import io.toro.clarizen.connector.ClarizenConnector
import io.toro.clarizen.exception.ClarizenException
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.lang.Stepwise

import io.toro.clarizen.model.request.*
import io.toro.clarizen.model.response.*
import io.toro.clarizen.model.common.*
import io.toro.clarizen.exception.ClarizenException

class EndPointDictionaryTest extends Specification {

	static {
		LicenseManagerProperties.setProductKeyProvider( new ProductKeyProvider( 'esb' ) );
		LicenseManagerProperties.setLicensePasswordProvider( new ToroPublicKeyPasswordProvider(
				'****COMMODORE64BASICV2****64KRAMSYSTEM38911BASICBYTESFREEREADY.' ) );
	}
	
	@Shared static String entityId
	@Shared static String workFlowId
	static String sessionId
	static String userId
	static String organizationId
	final static String alias = 'testAlias1'
	final static AccessCredentialRequest validAccessCredentialRequest = new AccessCredentialRequest([ 'userName': 'toro-tester4@live.com', 'password': 'alphAnumer1c' ])

	void entityExist( entityId, entitiesArray ) {
		assert entitiesArray*.id.contains( entityId ) == true
	}
	
	void saveSessionResponse( response ) {
		sessionId = response.sessionId
		userId = response.userId
		organizationId = response.organizationId
		
		assert sessionId && userId && organizationId
	}
	
	String pickRandomEntityId( String typeName ) {
		EntityQueryResponse response = ClarizenConnector.clarizenEntityQuery( alias, new EntityQueryRequest(
			typeName		: typeName ))
		int randomNumber = new Random().nextInt( response.entities.size() )
		response.entities[ randomNumber ].id
	}
	
	/*
	 * ServiceType - Authentication
	 *******************************/
	def 'authentication - login'() {
		LoginResponse response = ClarizenConnector.clarizenLogin( alias, validAccessCredentialRequest )

		expect: 
		saveSessionResponse( response )
	}
	
	def 'authentication - getServerDefinition'() {
		GetServerDefinitionResponse response = ClarizenConnector.clarizenGetServerDefinition( alias, validAccessCredentialRequest )

		expect:
		response.serverLocation                 == 'https://api2.clarizen.com/v2.0/services'
		response.appLocation                    == 'https://app2.clarizen.com/Clarizen'
	}
	
	def 'authentication - getSessionInfo'() {
		GetSessionInfoResponse response = ClarizenConnector.clarizenGetSessionInfo( alias,  new GetSessionInfoRequest() )

		expect:
		ClarizenConnector.clarizenGetSessionInfo( alias,  new GetSessionInfoRequest())
	}
	
	def 'authentication - logout'() {
		expect: ClarizenConnector.clarizenLogout( alias )
	}
	
	def 'login - again'() {
		LoginResponse response = ClarizenConnector.clarizenLogin( alias, validAccessCredentialRequest )

		expect: 
		saveSessionResponse( response )
	}
	
	/*
	 * ServiceType - Metadata
	 **************************/
	def 'metadata - describeEntities'() {
		expect:
		DescribeEntitiesResponse response = ClarizenConnector.clarizenDescribeEntities( alias, 'Issue' )
	}

	def 'metadata - describeEntityRelations'() {
		expect:
		DescribeEntityRelationsResponse response = ClarizenConnector.clarizenDescribeEntityRelations( alias, new DescribeEntityRelationsRequest([
			typeNames: [ 'Issue' ] ]))
	}

	def 'metadata - describeMetadata w/out get query' () {
		expect:
		DescribeMetadataResponse response = ClarizenConnector.clarizenDescribeMetadata( alias )
	}
	
	def 'metadata - describeMetadata'() {
		expect:
		DescribeMetadataResponse response = ClarizenConnector.clarizenDescribeMetadata( alias, 'WorkItem', 'relations,fields' )
	}

	def 'metadata - listEntities'() {
		expect:
		ListEntitiesResponse response = ClarizenConnector.clarizenListEntites( alias )
	}

	def 'metadata - objectsPut'() {
		ObjectsMetadataResponse response = ClarizenConnector.clarizenObjectsMetadataPut( alias, new ObjectsMetadataRequest([
			forType		: 'WorkItem',
			name		: 'My Rule',
			description	: 'This is my rule',
			triggerType	: 'CreateOrEdit',
			criteria	: 'False',
			action		: new Action([
				url			: 'http://www.example.org/',
				method		: HttpMethod.POST
				])
			]), 'Workflow' )
		workFlowId = response.id

		expect:
		workFlowId
	}

	def 'metadata - objectsDelete'() {
		expect:
		ClarizenConnector.clarizenObjectsMetadataDelete( alias, 'Workflow', workFlowId.substring(9))
	}

	def 'metadata - getSystemSettingsValue'() {
		expect:
		GetSystemSettingsValuesResponse response = ClarizenConnector.clarizenGetSystemSettingsValues( alias, 'Session Timeout, Lock out time' )
	}
	
	def 'metadata - setSystemSettingsValues'() {
		when:
		ClarizenConnector.clarizenSetSystemSettingsValue( alias, 'Outbound email notification address', 'true' )
		
		then: 'should throw error since we used trial account here'
		final ClarizenException exception = thrown()
		exception.message                       == 'General'
	}

	/*
	 * ServiceType - Data
	 ***********************/
	def 'data- objects - put'() {
		ObjectsDataPutResponse response = ClarizenConnector.clarizenObjectsDataPut( alias, 'issue', 'title', 'Issue99' )
		entityId = response.id

		expect:
		entityId
	}

	def 'data - objects - get'() {
		Map response = ClarizenConnector.clarizenObjectsDataGet( alias, 'issue', entityId.substring(6), 'CreatedOn,CreatedBy,CreatedBy.Name' )

		expect:
		response.id							== entityId
		response.CreatedBy.id.substring(6)	== userId
	}

	def 'data - objects - post'() {
		setup:
		ClarizenConnector.clarizenObjectsDataPost( alias, 'issue', entityId.substring(6), 'Priority', '5' )

		and: 'check the priority field'
		Map response = ClarizenConnector.clarizenObjectsDataGet( alias, 'issue', entityId.substring(6), 'CreatedOn,CreatedBy.Name,Priority' )

		expect:
		response.id						== entityId
		response.Priority				== 5
	}
	
	def 'data - aggregateQuery'() {
		expect:
		AggregateQueryResponse response = ClarizenConnector.clarizenAggregateQuery( alias, new AggregateQueryRequest([
			typeName			: 'Issue',
			aggregations		: [ new FieldAggregation([
				function 			: Function.Count,
				fieldName			: 'Title',
				alias				: 'Cnt'
				])],
			groupBy: [ 'State' ]
			]))
	}
	
	def 'data - changeState + data/objects PUT '() {
		setup: 'create an entity of type task'
		ObjectsDataPutResponse response = ClarizenConnector.clarizenObjectsDataPut( alias, 'task', 'name', 'TaskTest' )
		String entityId = response.id
	
		expect: 
		ClarizenConnector.clarizenChangeState( alias, new ChangeStateRequest([
			ids		: [ entityId ],
			state	: 'Completed'
			]))
	}
	
	def 'data - countQuery'() {
		expect:
		CountQueryResponse response = ClarizenConnector.clarizenCountQuery( alias, new CountQueryRequest(
			query : new EntityQuery(
				_type		: 'EntityQuery',
				typeName	: 'Issue',
				fields		: [ 'title' ],
				orders		: [ new OrderBy(
					fieldName	: 'title',
					order		: Order.Ascending
					)],
				deleted		: false
				)
			))
	}
	
	def 'data - createAndRetrieve'() {
		expect:
		CreateAndRetrieveResponse response = ClarizenConnector.clarizenCreateAndRetrieve( alias, new CreateAndRetrieveRequest([
			entity	: [
				Id			: '/Task',
				Name		: 'New Task',
				StartDate	: '2015-07-29',
				Duration	: [
					Unit		: 'Weeks',
					Value		: 3
				]],
			fields	: [ 'DueDate' ]
			]))
	}
	
	
	def 'data - createDiscussion'() {
		expect:
		CreateDiscussionResponse response = ClarizenConnector.clarizenCreateDiscussion( alias, new CreateDiscussionRequest([
			relatedEntities		: [ pickRandomEntityId( 'Task' ) ],
			notify				: [ pickRandomEntityId( 'User' ) ],
			topics				: [ pickRandomEntityId( 'Task' ) ],
			entity				: [
				id: '/DiscussionPost',
				body: 'Clarizen',
				Container: pickRandomEntityId( 'Issue' )]
			]))
	}

	def 'data - createFromTemplate'() {
		expect:
		CreateFromTemplateResponse response = ClarizenConnector.clarizenCreateFromTemplate( alias, new CreateFromTemplateRequest([
			templateName	: 'Your First Project',
			entity			: [
				id				: '/Project'
			]]))
	}

	def 'data - entityFeedQuery'() {
		expect:
		EntityFeedQueryResponse response = ClarizenConnector.clarizenEntityFeedQuery( alias, new EntityFeedQueryRequest([
			entityId		: pickRandomEntityId( 'Task' )
			]))
	}
	

	def 'data - entityQuery'() {
		setup: 'query the entities created after 2015-07-21'
		EntityQueryResponse response = ClarizenConnector.clarizenEntityQuery( alias, new EntityQueryRequest(
			typeName		: 'Issue',
			fields		: ['title', 'createdOn'],
			where			: new ConditionExpression(
				_type				: 'Compare',
				leftExpression	: new FieldExpression( fieldName: 'CreatedOn' ),
				operator			: Operator.GreaterThan,
				rightExpression	: new FieldExpression( value: '2015-07-30' )
				)
			))

		and: 'number of entities should be returned, check if the created entity exist'

		expect:
		response.entities.size() > 0
		entityExist( entityId, response.entities )
	}
	
	def 'data - findUserQuery'() {
		expect:
		FindUserQueryResponse response = ClarizenConnector.clarizenFindUserQuery( alias, new FindUserQueryRequest([
			firstName				: 'firstName',
			lastName				: 'lastName',
			eMail					: 'toro-tester3@live.com',
			fuzzySearchUserName		: true,
			includeSuspendedUsers	: true,
			fields					: ['eMail'],
			orders					: [ new OrderBy([
				order					: Order.Ascending,
				fieldName				: 'eMail'
				])],
			paging					: new Paging()
			]))
	}
	
	def 'data - getCalendarInfo'() {
		expect:
		GetCalendarInfoResponse response = ClarizenConnector.clarizenGetCalendarInfo( alias, userId )
	}

	def 'data - getTemplateDescriptions'() {
		expect:
		GetTemplateDescriptionsResponse response = ClarizenConnector.clarizenGetTemplateDescriptions( alias, 'Project' )
	}

	def 'data - groupsQuery'() {
		expect:
		GroupsQueryResponse response = ClarizenConnector.clarizenGroupsQuery( alias, new GroupsQueryRequest(
			fields : [ 'createdOn' ]
			))
	}
	
	def 'data - lifecycle'() {
		expect:
		ClarizenConnector.clarizenLifecycle( alias, new LifecycleRequest([
			ids	: [ pickRandomEntityId( 'Task' ) ],
			operation : 'Activate'
			]))
	}
	
	def 'data - newsFeedQuery'() {
		expect:
		NewsFeedQueryResponse response = ClarizenConnector.clarizenNewsFeedQuery( alias, new NewsFeedQueryRequest([
			mode : NewsFeedMode.All,
			fields : [ 'CreatedBy', 'CreatedOn' ]
			]))
	}

	def 'data - query'() {
		setup: 'query the entities created after 2015-07-21'
		QueryResponse response = ClarizenConnector.clarizenQuery ( alias, new QueryRequest([
			'q': 'SELECT createdOn,title FROM Issue WHERE createdOn>2015-07-30']))

		and: 'number of entities should be returned, check if the created entity exist'

		expect:
		response.entities.size() > 0
		entityExist( entityId, response.entities )
	}

	def 'data - relationQuery'() {
		expect:
		RelationQueryResponse response = ClarizenConnector.clarizenRelationQuery( alias, new RelationQueryRequest([
			entityId: pickRandomEntityId( 'Task' ),
			relationName: 'Issues'	
			]))
	}

	def 'data - repliesQuery'() {
		expect:
		RepliesQueryResponse response = ClarizenConnector.clarizenRepliesQuery( alias, new RepliesQueryRequest([
			postId : pickRandomEntityId( 'Task' )
			]))
	}
	
	def 'data - retrieveMultiple'() {
		expect:
		RetrieveMultipleResponse response = ClarizenConnector.clarizenRetrieveMultiple( alias, new RetrieveMultipleRequest([
			ids: [ '/Task/33pfowuv41rxobsdlux9b5xi44', '/Task/2mkvn6jgdphiw4q6b3q8mnfue4' ]
			]))
	}
	
	def 'data - search'() {
		expect:
		SearchResponse response = ClarizenConnector.clarizenSearch( alias, new SearchRequest([
			q : 'SELECT createdOn,title FROM Issue WHERE createdOn>2015-07-22'
			]))
	}
	
	def 'data - timesheetQuery'() {
		expect:
		TimesheetQueryResponse response = ClarizenConnector.clarizenTimeSheetQuery( alias, new TimesheetQueryRequest([
			projectId : '/Project/5saudzzzhr05ze7v6wch4osva1161'
			]))
	}
	
	/*
	 * ServiceType - Files
	 ***********************/
	def 'files - download'() {
		expect:
		ClarizenConnector.clarizenDownload( alias, pickRandomEntityId( 'Document' ), false )
	}
	
	@Shared String uploadUrl
	def 'files - getUploadUrl'() {
		GetUploadUrlResponse response = ClarizenConnector.clarizenGetUploadurl( alias )
		uploadUrl = response.uploadUrl
		
		expect:
		response.uploadUrl
	}
	
	def 'files - updateImage'() {
		expect:
		UpdateImageResponse response = ClarizenConnector.clarizenUpdateImage( alias, new UpdateImageRequest([
			entityId : pickRandomEntityId( 'Task' ),
			reset : true
			]))
	}
	
	def 'files - upload'() {
		expect:
		ClarizenConnector.clarizenUpload( alias, new UploadRequest([
			documentId : pickRandomEntityId( 'Document' ),
			fileInformation : new FileInformation([
				storage : 'Url',
				url : 'https://api2.clarizen.com/v2.0/services/files/23513665-635741002498528921_hLae4mj3mkWgT8ZudnJxsg-635741002498528921-9936BD33A001018CFDB7B4F0EEFB7C6F9B4B82FB/uploadFile',
				fileName : 'Lem',
				])
			]))

		
	}
	
	def 'objects - data - delete' () {
		given: 'existing entity'
		when: 'entity is deleted using entityId and queried'
		ClarizenConnector.clarizenObjectsDataDelete( alias, 'issue', entityId )
		Map response = ClarizenConnector.clarizenObjectsDataGet( alias, 'issue', entityId.substring(6), 'CreatedOn,CreatedBy,CreatedBy.Name' )

		then: 'should not be found by the entity getter method'
		final ClarizenException exception = thrown()
		exception.message == 'EntityNotFound'
	}
	
	/*
	 * ServiceType - Applications
	 *****************************/
	def 'applications - getApplicationStatus'() {
		expect:
		GetApplicationStatusResponse response = ClarizenConnector.clarizenGetApplicationStatus( alias, '8bc459da-c5c5-4b03-9530-175e7565f518' )
	}
	
	def 'applications - installApplication'() {
		expect:
		ClarizenConnector.clarizenInstallApplication( alias, new InstallApplicationRequest([
			applicationId : '8bc459da-c5c5-4b03-9530-175e7565f518',
			autoEnable : true
			]))
	}
	
	/*
	 * ServiceType - Bulk
	 *********************/
	def 'bulk - execute'() {
		ExecuteResponse response = ClarizenConnector.clarizenExecute( alias, new ExecuteRequest([ 
			requests:[
					new Request([
						url		:'/data/objects/Issue/6rogf9gdi2imf35es99lznp2s0',
						method	: 'POST',
						body	: '{title: "5"}' ]),
					new Request([
						url		: '/data/objects/Issue/6rogf9gdi2imf35es99lznp2s0?fields=duration',
						method	: 'GET']),
				]]))

		expect:
		response
	}
	
	/*
	 * ServiceType - Utils
	 **********************/
	def 'Utils - appLogin'() {
		expect:
		AppLoginResponse response = ClarizenConnector.clarizenAppLogin( alias )
	}
	
	def 'Utils - sendEMail'() {
		expect:
		ClarizenConnector.clarizenSendEmail( alias, new SendEmailRequest([
			subject : 'Try',
			body : 'body of the email',
			recipients : [ new Recipient([
				recipientType : 'To',
				user : pickRandomEntityId( 'User' ),
				eMail : 'toro-tester3@live.com'
				])],
			relatedEntity : pickRandomEntityId( 'Task' ),
			accessType : 'Public'
			]))
	}
}
