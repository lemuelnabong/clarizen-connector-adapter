import io.toro.clarizen.connector.ClarizenConnector
import io.toro.clarizen.exception.ClarizenException
import io.toro.clarizen.model.request.AccessCredentialRequest
import io.toro.clarizen.model.response.GetServerDefinitionResponse
import io.toro.clarizen.model.request.GetSessionInfoRequest
import io.toro.clarizen.model.response.GetSessionInfoResponse
import io.toro.clarizen.model.response.LoginResponse
import io.toro.clarizen.model.common.*
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import com.toro.licensing.LicenseManagerProperties
import com.toro.licensing.provider.ProductKeyProvider
import com.toro.licensing.provider.ToroPublicKeyPasswordProvider

@Stepwise
class SessionToroPropertyTest extends Specification {

	static {
		LicenseManagerProperties.setProductKeyProvider( new ProductKeyProvider( 'esb' ) );
		LicenseManagerProperties.setLicensePasswordProvider( new ToroPublicKeyPasswordProvider(
				'****COMMODORE64BASICV2****64KRAMSYSTEM38911BASICBYTESFREEREADY.' ) );
	}
	
	@Shared static String sessionId
	static String userId
	static String organizationId
	final static String alias = 'testAlias1'
	final static aliasOther = 'testAlias2'
	final static AccessCredentialRequest validAccessCredentialRequest = new AccessCredentialRequest([ 'userName': 'toro-tester4@live.com', 'password': 'alphAnumer1c' ])
	final static AccessCredentialRequest otherValidAccessCredentialRequest = new AccessCredentialRequest([ 'userName': 'toro-tester3@live.com', 'password': 'alphAnumer1c' ])
	final static AccessCredentialRequest invalidAccessCredentialRequest = new AccessCredentialRequest([ 'userName': 'unexistingUsername@live.com', 'password': 'wrongPassword' ])
		
	void saveSessionResponse( response ) {
		sessionId = response.sessionId
		userId = response.userId
		organizationId = response.organizationId
		
		assert sessionId && userId && organizationId
	}
	
	def 'login using invalid credentials'() {
		given: 'dead session'

		when: 'provided with invalid login credentials'
		ClarizenConnector.clarizenLogin( alias, invalidAccessCredentialRequest )

		then: 'should throw Login Failure'
		final ClarizenException exception = thrown()
		exception.message                       == 'LoginFailure'
	}

	def 'login using valid credentials'() {
		LoginResponse response = ClarizenConnector.clarizenLogin( alias, validAccessCredentialRequest )

		expect: 
		saveSessionResponse( response )
	}

	def 'invoke non-login methods using unexisting alias'() {
		when:
		ClarizenConnector.clarizenGetServerDefinition( 'unexistingAlias', validAccessCredentialRequest )

		then:
		final ClarizenException exception = thrown()
		exception.message						== 'InvalidTOROAlias'
	}

	def 'getServerDefinition with session active'() {
		expect:
		GetServerDefinitionResponse response = ClarizenConnector.clarizenGetServerDefinition( alias, validAccessCredentialRequest )
	}

	def 'getSessionInfo with active session'() {
		expect:
		GetSessionInfoResponse response = ClarizenConnector.clarizenGetSessionInfo( alias,  new GetSessionInfoRequest())
	}

	def 'login using otherValidCredentials'() {
		LoginResponse response = ClarizenConnector.clarizenLogin( aliasOther, otherValidAccessCredentialRequest )

		expect:
		response.sessionId
	}

	def 'getSessionInfo with active session again'() {
		GetSessionInfoResponse response = ClarizenConnector.clarizenGetSessionInfo( aliasOther,  new GetSessionInfoRequest() )

		expect:
		response.sessionId						!= sessionId
		response.userId							!= userId
	}

	def 'getSessionInfo using previous valid alias to map credentials from toro property'() {
		GetSessionInfoResponse response = ClarizenConnector.clarizenGetSessionInfo( alias,  new GetSessionInfoRequest() )

		expect:
		response.organizationId        			== organizationId
		response.userId							== userId
	}
}
