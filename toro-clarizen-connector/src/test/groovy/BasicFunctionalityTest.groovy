import com.toro.licensing.LicenseManagerProperties
import com.toro.licensing.provider.ProductKeyProvider
import com.toro.licensing.provider.ToroPublicKeyPasswordProvider
import io.toro.clarizen.connector.ClarizenConnector
import io.toro.clarizen.exception.ClarizenException
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.lang.Stepwise

import io.toro.clarizen.model.request.*
import io.toro.clarizen.model.response.*
import io.toro.clarizen.model.common.*
import io.toro.clarizen.exception.ClarizenException

@Stepwise
class BasicFunctionalityTest extends Specification {

	static {
		LicenseManagerProperties.setProductKeyProvider( new ProductKeyProvider( 'esb' ) );
		LicenseManagerProperties.setLicensePasswordProvider( new ToroPublicKeyPasswordProvider(
				'****COMMODORE64BASICV2****64KRAMSYSTEM38911BASICBYTESFREEREADY.' ) );
	}

	@Shared static String entityId
	final static String alias = 'testAlias1'
	static String sessionId
	static String userId
	static String organizationId
	final static AccessCredentialRequest validAccessCredentialRequest = new AccessCredentialRequest([ 'userName': 'toro-tester4@live.com', 'password': 'alphAnumer1c' ])

	void entityExist( entityId, entitiesArray ) {
		assert entitiesArray*.id.contains( entityId ) == true
	}

	void saveSessionResponse( response ) {
		sessionId = response.sessionId
		userId = response.userId
		organizationId = response.organizationId

		assert sessionId && userId && organizationId
	}

	/*
	 * Service Type - Aunthentication
	 *********************************/
    def 'login'() {
        LoginResponse response = ClarizenConnector.clarizenLogin( alias, validAccessCredentialRequest )

		expect:
		saveSessionResponse( response )
    }

	def 'getSessionInfo'() {
		expect:
		ClarizenConnector.clarizenGetSessionInfo( alias,  new GetSessionInfoRequest())
	}

	def 'logout'() {
		when:
		ClarizenConnector.clarizenLogout( alias )
		ClarizenConnector.clarizenGetSessionInfo( alias,  new GetSessionInfoRequest() )

		then:
		final ClarizenException exception = thrown()
		exception.message                       == 'SessionTimeout'
	}

	def 're-login'() {
		LoginResponse response = ClarizenConnector.clarizenLogin( alias, validAccessCredentialRequest )

		expect:
		response.sessionId
		response.organizationId         == organizationId
		response.userId                 == userId
	}

	/*
	 * Service Type - Metadata
	 **************************/
	def 'describeMetadata w/out get query' () {
		DescribeMetadataResponse response = ClarizenConnector.clarizenDescribeMetadata( alias )

		expect:
		response.entityDescriptions.size() > 0
	}

	def 'describeMetadata w/ get query'() {
		expect:
		DescribeMetadataResponse response = ClarizenConnector.clarizenDescribeMetadata( alias, 'WorkItem', 'relations,fields' )
	}

	/*
	 * Service Type - Data
	 ***********************/
	def 'create an entity issue'() {
		ObjectsDataPutResponse response = ClarizenConnector.clarizenObjectsDataPut( alias, 'issue', 'title', 'Issue99' )
		entityId = response.id

		expect:
		entityId
	}

	def 'get the entity object created'() {
		Map response = ClarizenConnector.clarizenObjectsDataGet( alias, 'issue', entityId.substring(6), 'CreatedOn,CreatedBy,CreatedBy.Name' )

		expect:
		response.id							== entityId
		response.CreatedBy.id.substring(6)	== userId
	}

	def 'update the entity priority'() {
		setup:
		ClarizenConnector.clarizenObjectsDataPost( alias, 'issue', entityId.substring(6), 'Priority', '5' )

		and: 'check the priority field'
		Map response = ClarizenConnector.clarizenObjectsDataGet( alias, 'issue', entityId.substring(6), 'CreatedOn,CreatedBy.Name,Priority' )

		expect:
		response.id						== entityId
		response.Priority				== 5
	}

	def 'query entities using CZQL'() {
		setup: 'query the entities created after 2015-07-30'
		QueryResponse response = ClarizenConnector.clarizenQuery ( alias, new QueryRequest([
			'q': 'SELECT createdOn,title FROM Issue WHERE createdOn>2015-07-21']))

		and: 'number of entities should be returned, check if the created entity exist'

		expect:
		response.entities.size() > 0
		entityExist( entityId, response.entities )
	}

	def 'query entities using entityQuery'() {
		setup: 'query the entities created after 2015-07-21'
		EntityQueryResponse response = ClarizenConnector.clarizenEntityQuery( alias, new EntityQueryRequest([
			'typeName'		: 'Issue',
			'fields'		: ['createdOn'],
			'where'			: new ConditionExpression([
				'_type'				: 'Compare',
				'leftExpression'	: new FieldExpression([ 'fieldName': 'CreatedOn' ]),
				'operator' 			: Operator.GreaterThan,
				'rightExpression'	: new FieldExpression([ 'value': '2015-07-21' ])
				])
			]))

		and: 'number of entities should be returned, check if the created entity exist'

		expect:
		response.entities.size() > 0
		entityExist( entityId, response.entities )
	}

	def 'query entities using CZQL with OutOfBoundDate'( String date ) {
		QueryResponse response = ClarizenConnector.clarizenQuery( alias, new QueryRequest([
			'q': 'SELECT createdOn,title FROM Issue WHERE createdOn>' + date ]))

		expect:
		response.entities.size() == 0

		where: date << [ '2015-08-21', '2018-07-22' ]
	}

	def 'deleteEntity and verify' () {
		given: 'existing entity'
		when: 'entity is deleted using entityId and queried'
		ClarizenConnector.clarizenObjectsDataDelete( alias, 'issue', entityId )
		Map response = ClarizenConnector.clarizenObjectsDataGet( alias, 'issue', entityId.substring(6), 'CreatedOn,CreatedBy,CreatedBy.Name' )

		then: 'should not be found by the entity getter method'
		final ClarizenException exception = thrown()
		exception.message == 'EntityNotFound'
	}

	/*
	 * Service Type - Bulk
	 ***********************/
	def 'execute'() {
		ExecuteResponse response = ClarizenConnector.clarizenExecute( alias, new ExecuteRequest([
			requests:[
					new Request([
						url		:'/data/objects/Issue/6rogf9gdi2imf35es99lznp2s0',
						method	: 'POST',
						body	: '{title: "2"}' ]),
					new Request([
						url		: '/data/objects/Issue/6rogf9gdi2imf35es99lznp2s0?fields=duration',
						method	: 'GET']),
				]]))

		expect:
		response
	}
}
