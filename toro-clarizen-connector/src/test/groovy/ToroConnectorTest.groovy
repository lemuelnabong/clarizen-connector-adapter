import io.toro.codenarc.ant.TOROCodenarcAntTask

/**
 * Created by daren on 19/06/15.
 * DO NOT DELETE THIS TEST FROM YOUR PROJECT. IT MUST ALWAYS BE USED
 */
class ToroConnectorTest extends TOROCodenarcAntTask {

    @Override
    String getRulesetFiles() {
        'rulesets/toro_connector_codenarc.xml'
    }

    @Override
    int getMaxPriority3Violations() {
        5
    }
}